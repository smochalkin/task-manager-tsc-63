<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.status.getDisplayName()}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td class="mini">
                <a href="/task/edit/?id=${task.id}">Edit</a>
            </td>
            <td class="mini">
                <a href="/task/delete/?id=${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/task/create">
    <button type="submit">Create</button>
</form>
<jsp:include page="../include/_footer.jsp" />