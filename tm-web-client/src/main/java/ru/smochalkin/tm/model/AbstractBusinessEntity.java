package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    protected Date endDate;

    protected Date startDate;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}