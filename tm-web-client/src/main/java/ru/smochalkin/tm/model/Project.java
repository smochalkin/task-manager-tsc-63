package ru.smochalkin.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractBusinessEntity {

    public Project(final String name) {
        this.name = name;
    }

}