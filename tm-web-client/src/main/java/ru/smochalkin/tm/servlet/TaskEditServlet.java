package ru.smochalkin.tm.servlet;

import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        req.setAttribute("viewName", "Edit task");
        req.setAttribute("task", TaskRepository.getInstance().findById(id));
        req.setAttribute("enumStatus", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final Status status = Status.getStatus(req.getParameter("status"));
        final String description = req.getParameter("description");
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setStatus(status);
        task.setDescription(description);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}