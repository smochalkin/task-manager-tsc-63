package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.repository.dto.ProjectRepository;
import ru.smochalkin.tm.repository.dto.TaskRepository;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class ProjectTaskDtoService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.bindTaskById(userId, projectId, taskId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.unbindTaskById(userId, projectId, taskId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.deleteByProjectId(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        ProjectDto projectDto = projectRepository.findFirstByUserIdAndName(userId, name);
        removeProjectById(projectDto.getId());
    }

}
