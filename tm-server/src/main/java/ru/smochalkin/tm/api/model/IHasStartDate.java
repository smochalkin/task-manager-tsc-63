package ru.smochalkin.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasStartDate {

    @Nullable
    Date getStartDate();

    void setStartDate(@Nullable final Date name);

}
